<h1>Project Elixir</h1>

Main system of smarthome defense system in containerized version. This is the development of my bachelor thesis<br>
<h3>What's inside?</h3>
<ul>
<li>Decoy & Bait (based on <i>alpine:latest</i> with <i>suricata</i> installed)</li>
<li>Main (based on <i>alpine:latest</i> with <i>nginx</i> installed)</li>
<li>DB (based on <i>mysql-5.7</i></li>
</ul>
<br>
Contacts : <b>aditya.kurniawan.95@gmail.com</b>